<?php
// Add custom Theme Functions here
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'mo-style', get_stylesheet_directory_uri() . '/style.css');
    wp_enqueue_style( 'mo-custom', get_stylesheet_directory_uri() . '/css/mo-custom.css');
    wp_enqueue_style( 'mo-font', get_stylesheet_directory_uri() . '/css/mo-font.css');
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

function wpbeginner_remove_version() {
return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');